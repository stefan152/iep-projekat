﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEP_projekat.Models;
using System.Web.Routing;

namespace IEP_projekat.Authorization_filters
{
    public class CurrentUserAuthorized: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Account currentUser = filterContext.HttpContext.Session["User"] as Account;

            var parameters = filterContext.ActionParameters;
            var id = parameters["id"];
            if (currentUser == null || id == null || currentUser.IdUser != (int)id)
            {
                RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                redirectTargetDictionary.Add("action", "Index");
                redirectTargetDictionary.Add("controller", "Home");
                redirectTargetDictionary.Add("area", "");

                filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
            }

            base.OnActionExecuting(filterContext);
        }
    }
}