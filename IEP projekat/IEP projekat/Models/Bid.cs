namespace IEP_projekat.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Bid")]
    public partial class Bid
    {
        [Key]
        public int IdBid { get; set; }

        public int IdUser { get; set; }

        public int IdAuction { get; set; }

        public DateTime Time { get; set; }

        public int Tokens { get; set; }

        public virtual Account Account { get; set; }

        public virtual Auction Auction { get; set; }
    }
}
