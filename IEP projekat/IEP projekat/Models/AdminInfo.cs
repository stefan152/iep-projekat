namespace IEP_projekat.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AdminInfo")]
    public partial class AdminInfo
    {
        [Key]
        public int IdAdminInfo { get; set; }

        public int ItemsPage { get; set; }

        public int DefaultDuration { get; set; }

        public int SilverPack { get; set; }

        public int GoldPack { get; set; }

        public int PlatinumPack { get; set; }

        [Required]
        [StringLength(5)]
        public string Currency { get; set; }

        public double TokenValue { get; set; }
    }
}
