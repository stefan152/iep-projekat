﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IEP_projekat.Models.Extended
{
    public class AuctionCreateExtended
    {
        [Required]
        [DisplayName("Name of the product")]
        [MaxLength(25, ErrorMessage = "{0} can have a max of {1} characters")]
        public string Product { get; set; }

        [DisplayName("Upload Photo")]
        public String ImagePath { get; set; }

        [Required]
        public HttpPostedFileBase ImageFile { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        [Display(Name = "Time (in seconds)")]
        public int Time { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        [Display(Name = "Price (in tokens)")]
        public int StartingPrice { get; set; }
    }
}