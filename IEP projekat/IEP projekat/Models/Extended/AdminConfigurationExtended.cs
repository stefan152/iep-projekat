﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace IEP_projekat.Models.Extended
{
    public class AdminConfigurationExtended
    {
        [Required]
        [Display(Name = "Items per page")]
        [Range(1, int.MaxValue, ErrorMessage = "This field must be a positive value")]
        public int ItemsPage { get; set; }

        [Required]
        [Display(Name = "Default duration")]
        [Range(1, int.MaxValue, ErrorMessage = "This field must be a positive value")]
        public int DefaultDuration { get; set; }

        [Required]
        [Display(Name = "Silver pack (tokens)")]
        [Range(1, int.MaxValue, ErrorMessage = "This field must be a positive value")]
        public int SilverPack { get; set; }

        [Required]
        [Display(Name = "Gold pack (tokens)")]
        [Range(1, int.MaxValue, ErrorMessage = "This field must be a positive value")]
        public int GoldPack { get; set; }

        [Required]
        [Display(Name = "Platinum pack (tokens)")]
        [Range(1, int.MaxValue, ErrorMessage = "This field must be a positive value")]
        public int PlatinumPack { get; set; }

        [Required]
        [StringLength(5)]
        public string Currency { get; set; }

        [Required]
        [Display(Name = "Value of token")]
        [Range(1, int.MaxValue, ErrorMessage = "This field must be a positive value")]
        public double TokenValue { get; set; }
    }
}