namespace IEP_projekat.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TokenOrder")]
    public partial class TokenOrder
    {
        [Key]
        public int IdOrder { get; set; }

        public int IdUser { get; set; }

        public int NumOfTokens { get; set; }

        public int Price { get; set; }

        [Required]
        [StringLength(10)]
        public string State { get; set; }

        public DateTime TimeSubmitted { get; set; }

        public Guid IdOrderGUID { get; set; }

        public virtual Account Account { get; set; }
    }
}
