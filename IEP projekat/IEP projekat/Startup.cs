﻿using Microsoft.Owin;
using Owin;
using IEP_projekat;

[assembly: OwinStartupAttribute(typeof(IEP_projekat.Startup))]
namespace IEP_projekat
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
