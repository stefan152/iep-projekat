﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace IEP_projekat.Hubs
{
    [Microsoft.AspNet.SignalR.Hubs.HubName("bidding")]
    public class Bidding : Hub
    {
        public void UpdateAuction(string idAuction, string newPrice, string leadingBidder)
        {
            Clients.All.updateAuction(idAuction, newPrice, leadingBidder);
        }

        public void NewBid(string leadingBidder,string date, string time, string newPrice)
        {
            Clients.All.newBid(leadingBidder, date, time, newPrice);
        }

        public void ReturnTokens(int previousBidder, string tokens)
        {
            Clients.All.returnTokens(previousBidder, tokens);
        }

        public void NewAuction(string notify)
        {
            Clients.All.newAuction(notify);
        }

        public void ReturnTokensNotifications(int previousBidder, string notify)
        {
            Clients.All.returnTokensNotifications(previousBidder, notify);
        }
    }
}