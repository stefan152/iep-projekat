﻿using IEP_projekat.Models;
using IEP_projekat.Models.Extended;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEP_projekat.Controllers
{
    public class HomeController : Controller
    {
        private IEPBaza myDB = new IEPBaza();

        public ActionResult Index()
        {
            return View();
        }

        [NonAction]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                myDB.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}