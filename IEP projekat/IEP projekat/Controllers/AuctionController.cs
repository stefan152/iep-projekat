﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEP_projekat.Models;
using IEP_projekat.Models.Extended;
using System.Net;
using PagedList;
using System.Data.Entity;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using IEP_projekat.Hubs;
using IEP_projekat.Authorization_filters;
using System.Net.Mail;
using System.Text;
using log4net;
using System.Text.RegularExpressions;

namespace IEP_projekat.Controllers
{
    public class AuctionController : Controller
    {
        private IEPBaza myDB = new IEPBaza();
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: Auction
        public ActionResult Index(string searchString, string currentFilter, string MinValue, string MaxValue, string ItemsPerPage, int? page)
        {
            //Ovo kasnije
            //var allCurrentAuctions = myDB.Auctions.Where(a => a.State.Equals("READY") == false).OrderByDescending(a => a.StartTime).ToList();
            var allCurrentAuctions = myDB.Auctions.Where(a=>a.State.Contains("READY") == false).OrderByDescending(a => a.StartTime).OrderByDescending(a=>a.State).ToList();
            
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            if (!String.IsNullOrEmpty(searchString))
            {
                String myString = searchString;
                myString = Regex.Replace(myString, @"\s+", " ");

                foreach (var word in myString.Split(' ')) {
                    allCurrentAuctions = allCurrentAuctions.Where(a => a.Product.ToLower().Contains(word.ToLower())).ToList();
                }
            }

            ViewBag.MinValue = null;
            int min;
            if (!String.IsNullOrEmpty(MinValue) && int.TryParse(MinValue, out min))
            {
                allCurrentAuctions = allCurrentAuctions.Where(a => a.EndPrice >= min).ToList();
                ViewBag.MinValue = min;
            }

            ViewBag.MaxValue = null;
            int max;
            if (!String.IsNullOrEmpty(MaxValue) && int.TryParse(MaxValue, out max))
            {
                allCurrentAuctions = allCurrentAuctions.Where(a => a.EndPrice <= max).ToList();
                ViewBag.MaxValue = max;
            }

            int items;
            //ViewBag.ItemsPerPage = ItemsPerPage;
            AdminInfo adminInfo = myDB.AdminInfoes.FirstOrDefault();
            ViewBag.ItemsPerPage = (int.TryParse(ItemsPerPage, out items) && items > 0) ? items : adminInfo.ItemsPage;
            //if (!(!String.IsNullOrEmpty(ItemsPerPage) && int.TryParse(ItemsPerPage, out items)))
            //{
            //    items = adminInfo.ItemsPage;
            //    ViewBag.ItemsPerPage = items;
            //}

            int pageNumber = (page ?? 1);
            return View(allCurrentAuctions.ToPagedList(pageNumber, (int)ViewBag.ItemsPerPage));
        }

        [UserAuthorized]
        public ActionResult CreateAuction()
        {
            try
            {
                AdminInfo adminInfo = myDB.AdminInfoes.FirstOrDefault();
                Session["price"] = (int) adminInfo.TokenValue;
                Session["currency"] = adminInfo.Currency;
                return View();
            }
            catch (Exception ex)
            {
                log.Info("Auction/UserAuction No argument for method \n Exception text:" + ex.ToString());
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            } 
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorized]
        public ActionResult CreateAuction([Bind(Include = "Product, Time, StartingPrice, ImagePath, ImageFile")] AuctionCreateExtended model)
        {
            if (ModelState.IsValid && model.ImageFile.ContentLength > 0)
            {
                {
                    byte[] myImage = new byte[model.ImageFile.ContentLength];
                    model.ImageFile.InputStream.Read(myImage, 0, myImage.Length);

                    Account currentUser = Session["User"] as Account;
                    if (currentUser == null)
                    {
                        log.Info("Auction/CreateAuction No current session user");
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }

                    Auction auction = new Auction
                    {
                        IdUser = currentUser.IdUser,
                        Product = model.Product,
                        CreateTime = System.DateTime.Now,
                        StartTime = System.DateTime.Now,
                        EndTime = System.DateTime.Now,
                        Time = model.Time,
                        BeginningPrice = model.StartingPrice,
                        EndPrice = model.StartingPrice,
                        State = "READY",
                        Image = myImage,
                        IdAuctionGUID = Guid.NewGuid()
                    };

                    myDB.Auctions.Add(auction);
                    myDB.SaveChanges();

                    return RedirectToAction("Index", "Auction");
                }
            }
            return View();
        }

        [UserAuthorized]
        public ActionResult UserAuction(int? page, int? id)
        {
            if (id.HasValue)
            {
                //Uzimaju se sve aukcije koje pravi trenutni korisnik
                var auctions = myDB.Auctions.Where(a => a.IdUser == id).ToList();
                AdminInfo adminInfo = myDB.AdminInfoes.FirstOrDefault();

                int items = adminInfo.ItemsPage;
                int pageNumber = (page ?? 1);
                return View(auctions.ToPagedList(pageNumber, items));
            }

            log.Info("Auction/UserAuction No argument for method");
            //Doslo je do grske cim nije data i identifikacija trenutnog korisnika
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [UserAuthorized]
        public ActionResult WonAuctions(int? page, int? id)
        {
            if (id.HasValue)
            {
                List<Auction> wonAuctions = new List<Auction>();
                var auctions = myDB.Auctions.Where(a => a.State.Equals("COMPLETED")).ToList();
                foreach (var item in auctions)
                {
                    Bid curBid = myDB.Bids.Where(b => b.IdAuction == item.IdAuction).OrderByDescending(b => b.Tokens).FirstOrDefault();
                    if (curBid != null && curBid.IdUser == id)
                    {
                        wonAuctions.Add(item);
                    }
                }
                AdminInfo adminInfo = myDB.AdminInfoes.FirstOrDefault();
                int items = adminInfo.ItemsPage;

                int pageNumber = (page ?? 1);
                return View(wonAuctions.ToPagedList(pageNumber, items));
            }

            log.Info("Auction/WonAuctiona No argument for method");
            //Doslo je do greske cim nije data i identifikacija trenutnog korisnika
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [AdminAuthorized]
        public ActionResult NeedAdminApproval(int? page)
        {
            var auctions = myDB.Auctions.Where(a => a.State.Equals("READY")).ToList();

            AdminInfo adminInfo = myDB.AdminInfoes.FirstOrDefault();
            int items = adminInfo.ItemsPage;

            int pageNumber = (page ?? 1);
            return View(auctions.ToPagedList(pageNumber, items));
        }

        [AdminAuthorized]
        public ActionResult OpenAuction(int? id)
        {
            if (id.HasValue)
            {
                Auction auction = myDB.Auctions.Where(a => a.IdAuction == id).FirstOrDefault();
                if (auction != null)
                {
                    auction.State = "OPENED";
                    auction.StartTime = System.DateTime.Now;
                    auction.EndPrice = auction.BeginningPrice;

                    myDB.Entry(auction).State = EntityState.Modified;
                    myDB.SaveChanges();

                    var hubContext = GlobalHost.ConnectionManager.GetHubContext<Bidding>();
                    string notification = "New auction just started. If you don't see it please click <a href = " + Url.Action("Index", "Auction", null) + ">here</a>";
                    hubContext.Clients.All.newAuction(notification);

                    return RedirectToAction("NeedAdminApproval");
                }

                log.Info("Auction/UserAuction Auction not found in DB");
                return HttpNotFound();
            }

            log.Info("Auction/OpenAuction No argument for method");
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [AdminAuthorized]
        public ActionResult RejectAuction(int? id)
        {
            if (id.HasValue)
            {
                Auction auction = myDB.Auctions.Where(a => a.IdAuction == id).FirstOrDefault();
                if (auction != null)
                {
                    myDB.Auctions.Remove(auction);
                    myDB.SaveChanges();

                    return RedirectToAction("NeedAdminApproval");
                }
                //Ne postoji u bazi aukcija sa datim id-jem
                log.Info("Auction/RejectAuction Auction not found in DB");
                return HttpNotFound();
            }

            log.Info("Auction/RejectAuction No argument for method");
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        public ActionResult Details(int? id)
        {
            if (id.HasValue)
            {
                Auction auction = myDB.Auctions.Where(a => a.IdAuction == id).FirstOrDefault();
                if (auction != null)
                {
                    //Prvo se nade korisnik koji je napravio datu aukciju
                    //i postavi se u ViewBag radi lakseg prikazivanja
                    FindMaker(auction.IdUser);
                    return View(auction);
                }
                //Ne postoji u bazi aukcija sa datim id-jem
                log.Info("Auction/Details Auction not found in DB");
                return HttpNotFound();
            }
            log.Info("Auction/Details No argument for method");
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [AdminAuthorized]
        public ActionResult NeedAdminApprovalDetails(int? id)
        {
            if (id.HasValue)
            {
                Auction auction = myDB.Auctions.Where(a => a.IdAuction == id).FirstOrDefault();
                if (auction != null)
                {
                    //Prvo se nade korisnik koji je napravio datu aukciju
                    //i postavi se u ViewBag radi lakseg prikazivanja
                    FindMaker(auction.IdUser);
                    return View(auction);
                }
                //Ne postoji u bazi aukcija sa datim id-jem
                log.Info("Auction/NeedAdminApprovalDetails Auction not found in DB");
                return HttpNotFound();
            }

            log.Info("Auction/NeedAdminApprovalDetails No argument for method");
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [UserAuthorized]
        public async void MakeCustomBid(int curAuction, int numOfTokensBidded)
        {
            Account currentUser = Session["User"] as Account;
            if (currentUser == null)
            {
                log.Info("Auction/MakeCustomBid No current user in session");
                return;
            }

            using (var transaction = myDB.Database.BeginTransaction(IsolationLevel.Serializable))
            {
                try
                {
                    Auction auctionOfInterest = myDB.Auctions.Where(a => a.IdAuction == curAuction && a.State.Equals("COMPLETE") == false).FirstOrDefault();
                    if (auctionOfInterest == null)
                    {
                        log.Info("Auction/MakeCustomBid No auction found");
                        return;
                    }
                 
                    //Uzima se poslednja transakcija iz tabele
                    Bid winngingBid = myDB.Bids.Where(b => b.IdAuction == curAuction).OrderByDescending(b => b.Tokens).FirstOrDefault();
                    Account curUserModel = myDB.Accounts.Find(currentUser.IdUser);
                    Account previousBidder = null;
                    Account returnTokensAccount = null;
                    if (winngingBid != null)
                        previousBidder = myDB.Accounts.Find(winngingBid.IdUser);

                    int currentPriceInTokens;
                    if (winngingBid != null)
                        currentPriceInTokens = winngingBid.Tokens;
                    else
                        currentPriceInTokens = auctionOfInterest.EndPrice - 1;

                    //Mora za jedan veca vrednost neko pretkodno
                    if (currentPriceInTokens + 1 > numOfTokensBidded)
                    {
                        ViewBag.Message = "Your bid is too small! Bid higher if you want to win";
                        return;
                    }

                    if (curUserModel.Tokens - currentPriceInTokens - numOfTokensBidded <= 0)
                    {
                        //stavi da se ispise i link ka kupovini jos tokena odmah ispise
                        TempData["message"] = "You don't have enough tokens! Click <a href=" + Url.Action("Create", "TokenOrder", null) + ">here</a> to buy more tokens";
                        return;
                    }

                    Bid leadingBid = new Bid
                    {
                        IdUser = curUserModel.IdUser,
                        IdAuction = curAuction,
                        Time = System.DateTime.Now,
                        Tokens = numOfTokensBidded
                    };

                    if (previousBidder != null)
                        returnTokensAccount = previousBidder;

                    if (returnTokensAccount != null)
                    {
                        returnTokensAccount.Tokens += winngingBid.Tokens;
                        myDB.Entry(returnTokensAccount).State = EntityState.Modified;
                    }

                    curUserModel.Tokens -= leadingBid.Tokens;
                    myDB.Entry(curUserModel).State = EntityState.Modified;

                    Session["User"] = curUserModel;

                    myDB.Bids.Add(leadingBid);
                    auctionOfInterest.EndPrice = leadingBid.Tokens;
                    myDB.Entry(auctionOfInterest).State = EntityState.Modified;
                    await myDB.SaveChangesAsync();

                    transaction.Commit();

                    var hubContext = GlobalHost.ConnectionManager.GetHubContext<Bidding>();

                    hubContext.Clients.All.updateAuction(auctionOfInterest.IdAuction, auctionOfInterest.EndPrice, curUserModel.Email);
                    hubContext.Clients.All.newBid(currentUser.Email, System.DateTime.Now.ToString("d/M/yyyy"), System.DateTime.Now.ToString("hh:mm:ss"), leadingBid.Tokens);

                    if (returnTokensAccount != null && returnTokensAccount.IdUser != currentUser.IdUser)
                    {
                        hubContext.Clients.All.returnTokens(returnTokensAccount.IdUser, returnTokensAccount.Tokens);
                        string notification = "You have been outbided. Go to the <a href= " + Url.Action("Details", "Auction", new { id = auctionOfInterest.IdAuction }) + "> auction </a> ";
                        hubContext.Clients.All.returnTokensNotifications(returnTokensAccount.IdUser, notification);
                    }
                    //skace se na detalje o aukciji o kojoj se radi
                    return;
                }
                catch (Exception ex)
                {
                    log.Info("Auction/MakeBid Error in tansaction/SignalR \n Exception text:" + ex.ToString());
                    transaction.Rollback();
                    return;
                }
            }
        }


        [UserAuthorized]
        public async Task<ActionResult> MakeBid(int curAuction)
        {
            Account currentUser = Session["User"] as Account;
            if (currentUser == null)
            {
                log.Info("Auction/MakeBid No current user in session");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (var transaction = myDB.Database.BeginTransaction(IsolationLevel.Serializable))
            {
                try
                {
                    Auction auctionOfInterest = myDB.Auctions.Where(a => a.IdAuction == curAuction && a.State.Equals("COMPLETE") == false).FirstOrDefault();
                    if (auctionOfInterest == null)
                        return HttpNotFound();

                    //Uzima se poslednja transakcija iz tabele
                    Bid winngingBid = myDB.Bids.Where(b => b.IdAuction == curAuction).OrderByDescending(b => b.Tokens).FirstOrDefault();
                    Account curUserModel = myDB.Accounts.Find(currentUser.IdUser);
                    Account previousBidder = null;
                    Account returnTokensAccount = null;
                    if (winngingBid != null)
                        previousBidder = myDB.Accounts.Find(winngingBid.IdUser);

                    int currentPriceInTokens;
                    if (winngingBid != null)
                        currentPriceInTokens = winngingBid.Tokens;
                    else
                        currentPriceInTokens = auctionOfInterest.EndPrice - 1;

                    //Mora za jedan veca vrednost neko pretkodno
                    if (curUserModel.Tokens - currentPriceInTokens - 1 <= 0)
                    {
                        //stavi da se ispise i link ka kupovini jos tokena odmah ispise
                        TempData["message"] = "You don't have enough tokens!";
                        return RedirectToAction("Index");
                    }

                    Bid leadingBid = new Bid
                    {
                        IdUser = curUserModel.IdUser,
                        IdAuction = curAuction,
                        Time = System.DateTime.Now,
                        Tokens = currentPriceInTokens + 1
                    };

                    if (previousBidder != null)
                        returnTokensAccount = previousBidder;

                    if (returnTokensAccount != null)
                    {
                        returnTokensAccount.Tokens += winngingBid.Tokens;
                        myDB.Entry(returnTokensAccount).State = EntityState.Modified;
                    }

                    curUserModel.Tokens -= leadingBid.Tokens;
                    myDB.Entry(curUserModel).State = EntityState.Modified;

                    Session["User"] = curUserModel;

                    myDB.Bids.Add(leadingBid);
                    auctionOfInterest.EndPrice = leadingBid.Tokens;
                    myDB.Entry(auctionOfInterest).State = EntityState.Modified;
                    await myDB.SaveChangesAsync();

                    transaction.Commit();

                    var hubContext = GlobalHost.ConnectionManager.GetHubContext<Bidding>();

                    hubContext.Clients.All.updateAuction(auctionOfInterest.IdAuction, auctionOfInterest.EndPrice, curUserModel.Email);
                    hubContext.Clients.All.newBid(currentUser.Email, System.DateTime.Now.ToString("d/M/yyyy"), System.DateTime.Now.ToString("hh:mm:ss"), leadingBid.Tokens);

                    if (returnTokensAccount != null && returnTokensAccount.IdUser != currentUser.IdUser)
                    {
                        hubContext.Clients.All.returnTokens(returnTokensAccount.IdUser, returnTokensAccount.Tokens);
                        string notification = "You have been outbided. Go to the <a href= " + Url.Action("Details", "Auction", new { id = auctionOfInterest.IdAuction }) + "> auction </a> ";
                        hubContext.Clients.All.returnTokensNotifications(returnTokensAccount.IdUser, notification);
                    }
                    //skace se na detalje o aukciji o kojoj se radi
                    return RedirectToAction("Details", "Auction", new { id = auctionOfInterest.IdAuction });
                }
                catch (Exception ex)
                {
                    log.Info("Auction/MakeBid Error in tansaction/SignalR \n Exception text:" + ex.ToString());
                    transaction.Rollback();
                    return RedirectToAction("Index", "Auction");

                    //return RedirectToAction("Index");
                }
            }
        }

        public void CompleteAuction(int? id)
        {
            if (id.HasValue)
            {
                using (var transaction = myDB.Database.BeginTransaction(IsolationLevel.Serializable))
                {
                    try
                    {
                        Auction finishedAuction = myDB.Auctions.Find(id);
                        if (finishedAuction != null && !finishedAuction.State.Contains("COMPLETED"))
                        {
                            finishedAuction.EndTime = System.DateTime.Now;
                            finishedAuction.State = "COMPLETED";

                            myDB.Entry(finishedAuction).State = EntityState.Modified;
                            myDB.SaveChanges();

                            SendEmailToWinner(id);

                            transaction.Commit();
                        }
                        log.Info("Auction/CompleteAuction Auction not found in DB");
                    }
                    catch (Exception ex)
                    {
                        log.Info("Auction/CompleteAuction Error in tansaction \n Exception text:" + ex.ToString());
                        transaction.Rollback();
                    }
                }
              
            }
            log.Info("Auction/CompleteAuction No argument for method");
        }

        [NonAction]
        public void SendEmailToWinner(int? id)
        {
            if (id.HasValue)
            {
                Bid winningBid = myDB.Bids.Where(b => b.IdAuction == id).OrderByDescending(b => b.Tokens).FirstOrDefault();
                Account winner = myDB.Accounts.Find(winningBid.IdUser);
                Auction wonAuctions = myDB.Auctions.Find(id);

                string subject = "Congratilation on winning auction id=" + id;
                string body = "Dear " + winner.FirstName + " " + winner.LastName + ",<br />" + 
                              "We are happy to announce that you have won the item " + wonAuctions.Product + "<br />" +
                              "The price was: " + wonAuctions.EndPrice + " tokens<br />" +
                              "Thank you for using our web site.See you in another auction. Good luck!<br /><br />" +
                              "Sincerely,<br/>" +
                              "Auction mania";

                bool status = SendEmail(winner, subject, body);
            }
        }

        [NonAction]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                myDB.Dispose();
            }
            base.Dispose(disposing);
        }

        [NonAction]
        public void FindMaker(int id)
        {
            Account account = myDB.Accounts.Where(a => a.IdUser == id).FirstOrDefault();
            ViewBag.Maker = account.FirstName + " " + account.LastName;
        }

        [NonAction]
        public bool SendEmail(Account reciver, string subject, string emailBody)
        {
            try
            {
                string senderEmail = System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString();
                string senderPassword = System.Configuration.ConfigurationManager.AppSettings["SenderPassword"].ToString();

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Timeout = 100000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(senderEmail, senderPassword);

                MailMessage mailMessage = new MailMessage(senderEmail, reciver.Email, subject, emailBody);
                mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = UTF8Encoding.UTF8;

                client.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                log.Info("Auction/SendEmail Error sending e-mail");
                return false;
            }
        }
    }
}
