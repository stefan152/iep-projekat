﻿using IEP_projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IEP_projekat.Models.Extended;
using IEP_projekat.Models;
using System.Data.Entity;
using IEP_projekat.Authorization_filters;
using log4net;

namespace IEP_projekat.Controllers
{
    public class AdminConfigurationController : Controller
    {
        private IEPBaza myDB = new IEPBaza();
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [AdminAuthorized]
        public ActionResult Index()
        {
            return RedirectToAction("ViewAdminInfo");
        }

        [AdminAuthorized]
        public ActionResult ViewAdminInfo()
        {
            AdminInfo adminInfo = myDB.AdminInfoes.FirstOrDefault();

            if (adminInfo != null)
            {
                return View(new AdminConfigurationExtended {
                    ItemsPage = adminInfo.ItemsPage,
                    DefaultDuration = adminInfo.DefaultDuration,
                    SilverPack = adminInfo.SilverPack,
                    GoldPack = adminInfo.GoldPack,
                    PlatinumPack = adminInfo.PlatinumPack,
                    Currency = adminInfo.Currency,
                    TokenValue = adminInfo.TokenValue
                });
            }

            log.Info("AdminConfiguration/ViewAdminInfo AdminInfo not found in DB");
            return HttpNotFound();
        }

        [AdminAuthorized]
        public ActionResult EditAdminInfo()
        {
            AdminInfo adminInfo = myDB.AdminInfoes.FirstOrDefault();

            if (adminInfo != null)
            {
                return View(new AdminConfigurationExtended
                {
                    ItemsPage = adminInfo.ItemsPage,
                    DefaultDuration = adminInfo.DefaultDuration,
                    SilverPack = adminInfo.SilverPack,
                    GoldPack = adminInfo.GoldPack,
                    PlatinumPack = adminInfo.PlatinumPack,
                    Currency = adminInfo.Currency,
                    TokenValue = adminInfo.TokenValue
                });
            }

            log.Info("AdminConfiguration/EditAdminInfo AdminInfo not found in DB");
            return HttpNotFound();     
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AdminAuthorized]
        public ActionResult EditAdminInfo([Bind(Include = "ItemsPage, DefaultDuration, SilverPack, GoldPack, PlatinumPack, Currency, TokenValue")] AdminConfigurationExtended model)
        {
            if (ModelState.IsValid)
            {
                AdminInfo adminInfo = myDB.AdminInfoes.FirstOrDefault();
                if (adminInfo != null)
                {
                    if (!((model.SilverPack < model.GoldPack) && (model.GoldPack < model.PlatinumPack)))
                    {
                        ViewBag.Message = "Silver pack must be the smallest, then the Gold pack, then Platinum pack must be the biggest pack of them all.";
                        return View(model);
                    }

                    adminInfo.ItemsPage = model.ItemsPage;
                    adminInfo.DefaultDuration = model.DefaultDuration;
                    adminInfo.SilverPack = model.SilverPack;
                    adminInfo.GoldPack = model.GoldPack;
                    adminInfo.PlatinumPack = model.PlatinumPack;
                    adminInfo.Currency = model.Currency;
                    adminInfo.TokenValue = model.TokenValue;

                    myDB.Entry(adminInfo).State = EntityState.Modified;
                    myDB.SaveChanges();
                    return RedirectToAction("ViewAdminInfo","AdminConfiguration");
                }
            }

            return View();
        }

        [NonAction]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                myDB.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}