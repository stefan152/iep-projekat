﻿using IEP_projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IEP_projekat.Models;
using System.Data.Entity;
using IEP_projekat.Models.Extended;
using System.Data;
using PagedList;
using IEP_projekat.Authorization_filters;
using System.Net.Mail;
using System.Text;
using log4net;

namespace IEP_projekat.Controllers
{
    public class TokenOrderController : Controller
    {
        private IEPBaza myDB = new IEPBaza();
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string CENTILI_SUCCESS = "success";
        private const string CENTILI_FAIL = "fail";

        [UserAuthorized]
        public ActionResult Index(int? page)
        {
            Account currentUser = Session["User"] as Account;
            if (currentUser != null)
            {
                //Ibacuju se sve narudzbine po opadajucem redosledu datuma
                var tokenOrders = myDB.TokenOrders.Where(t => t.IdUser == currentUser.IdUser).OrderByDescending(t=>t.TimeSubmitted).ToList();

                AdminInfo adminInfo = myDB.AdminInfoes.FirstOrDefault();
                int items = adminInfo.ItemsPage;

                int pageNumber = (page ?? 1);
                return View(tokenOrders.ToPagedList(pageNumber, items));
            }

            log.Info("TokenOrder/Index No logged in user");
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [UserAuthorized]
        public ActionResult Details(int? id)
        {
            if (id.HasValue)
            {
                TokenOrder myTokenOrder = myDB.TokenOrders.Find(id);
                if (myTokenOrder != null)
                {
                    return View(myTokenOrder);
                }

                return HttpNotFound();
            }

            log.Info("TokenOrder/Details No argumetn for method");
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [UserAuthorized]
        public ActionResult Create()
        {
            AdminInfo adminInfo = myDB.AdminInfoes.FirstOrDefault();
            Session["silver"] = adminInfo.SilverPack;
            Session["gold"] = adminInfo.GoldPack;
            Session["platinum"] = adminInfo.PlatinumPack;
            Session["price"] = (int)adminInfo.TokenValue;
            Session["currency"] = (String)adminInfo.Currency;
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CurrentUserAuthorized]
        public ActionResult Create(int id, string pack)
        {
            Account currentUser = Session["User"] as Account;
            Account myUser = myDB.Accounts.Find(id);
            AdminInfo adminInfo = myDB.AdminInfoes.FirstOrDefault();

            Session["silver"] = adminInfo.SilverPack;
            Session["gold"] = adminInfo.GoldPack;
            Session["platinum"] = adminInfo.PlatinumPack;
            Session["price"] = (int)adminInfo.TokenValue;
            Session["currency"] = (String)adminInfo.Currency;

            if (currentUser != null && adminInfo != null && currentUser.IdUser == myUser.IdUser)
            {
                int numberOfTokens = 0;
                double newPrice = 0;
                switch (pack)
                {
                    case "silver":
                        numberOfTokens = adminInfo.SilverPack;
                        //popust od 5%
                        newPrice = numberOfTokens * adminInfo.TokenValue * 0.95;
                        break;
                    case "gold":
                        numberOfTokens = adminInfo.GoldPack;
                        //popust od 10%
                        newPrice = numberOfTokens * adminInfo.TokenValue * 0.90;
                        break;
                    case "platinum":
                        numberOfTokens = adminInfo.PlatinumPack;
                        //popust od 15%
                        newPrice = numberOfTokens * adminInfo.TokenValue * 0.85;
                        break;
                }

                TokenOrder newTokenOrder = new TokenOrder
                {
                    IdUser = myUser.IdUser,
                    NumOfTokens = numberOfTokens,
                    Price = (int)newPrice,
                    State = "SUBMITED",
                    TimeSubmitted = System.DateTime.Now,
                    IdOrderGUID = Guid.NewGuid()
            };

                myDB.TokenOrders.Add(newTokenOrder);
                myDB.SaveChanges();

                return Redirect("http://stage.centili.com/payment/widget?apikey=5e312449d51251970f17be58787d70cf&country=rs&reference=" + newTokenOrder.IdOrderGUID);
            }

            return View();
        }
        
        public ActionResult PaymentCompleted(Guid reference, string status)
        {
            using (var trans = myDB.Database.BeginTransaction(IsolationLevel.Serializable))
            {
                TokenOrder tokenOrder = myDB.TokenOrders.Where(t => t.IdOrderGUID == reference).FirstOrDefault();
                Account myUser = myDB.Accounts.Where(a => a.IdUser == tokenOrder.IdUser).FirstOrDefault();
                Account currentUser = Session["User"] as Account;

                if (tokenOrder != null && currentUser != null && currentUser.IdUser == myUser.IdUser)
                {
                    try
                    {
                        if (status == CENTILI_SUCCESS)
                        {
                            tokenOrder.State = "COMPLETED";
                            myUser.Tokens += tokenOrder.NumOfTokens;
                            Session["User"] = myUser;
                            SendEmailToBuyerSuccess(myUser, tokenOrder);
                        }
                        else
                        {
                            tokenOrder.State = "CANCELED";
                            SendEmailToBuyerFail(myUser, tokenOrder);
                        }

                        myDB.Entry(myUser).State = EntityState.Modified;
                        myDB.SaveChanges();
                        myDB.Entry(tokenOrder).State = EntityState.Modified;
                        myDB.SaveChanges();
                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        log.Info("TokenOrder/PaymentCompleted Exception with Centili payment");
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                }

                return RedirectToAction("Details", "User", new { id = myUser.IdUser });
            }
        }

        [NonAction]
        public void SendEmailToBuyerSuccess(Account reciver, TokenOrder myTokenOrder)
        {
            string emailSubject = "Payment successful!";
            string emailBody = "Dear " + reciver.FirstName + " " + reciver.LastName + ", <br/>" +
                                "Your payment was successful! You bought " + myTokenOrder.NumOfTokens + " tokens. Use them wisely!<br /> " +
                                "Sincerely, <br/>" +
                                "Auction mania";
            bool status = SendEmail(reciver, emailSubject, emailBody);
        }

        [NonAction]
        public void SendEmailToBuyerFail(Account reciver, TokenOrder myTokenOrder)
        {
            string emailSubject = "Payment UNsuccessful!";
            string emailBody = "Dear " + reciver.FirstName + " " + reciver.LastName + ", <br/>" +
                                "Your payment was unsuccessful! Please try again later. We are sorry for the inconvinience. <br /> " +
                                "Sincerely, <br/>" +
                                "Auction mania";
            bool status = SendEmail(reciver, emailSubject, emailBody);
        }

        [NonAction]
        public bool SendEmail(Account reciver, string subject, string emailBody)
        {
            try
            {
                string senderEmail = System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString();
                string senderPassword = System.Configuration.ConfigurationManager.AppSettings["SenderPassword"].ToString();

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Timeout = 100000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(senderEmail, senderPassword);

                MailMessage mailMessage = new MailMessage(senderEmail, reciver.Email, subject, emailBody);
                mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = UTF8Encoding.UTF8;

                client.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                log.Info("TokenOrder/SendEmail Error with mail sending");
                return false;
            }
        }

        [NonAction]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                myDB.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}