﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using IEP_projekat.Models;
using IEP_projekat.Models.Extended;
using System.Text;
using System.Net;
using System.Data.Entity;
using IEP_projekat.Authorization_filters;
using log4net;

namespace IEP_projekat.Controllers
{
    public class UserController : Controller
    {
        private IEPBaza myDB = new IEPBaza();
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ActionResult Index()
        {
            return View();
        }

        private string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public ActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Include = "FirstName, LastName, Email, Password, ConfirmPassword")] RegistrationExtended model)
        {
            if (ModelState.IsValid)
            {
                if (validateEmail(model.Email))
                {
                    string password = "";
                    using (MD5 md5Hash = MD5.Create())
                    {
                        password = GetMd5Hash(md5Hash, model.Password);
                    }

                    Account account = new Account
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Password = password,
                        Email = model.Email,
                        Tokens = 0,
                        Role = "USER"
                    };

                    myDB.Accounts.Add(account);
                    myDB.SaveChanges();

                    Session["User"] = account;
                    return RedirectToAction("Index", "Auction");
                }
                else
                {
                    //Poruka o vec postojecem mejlu i da se posalje na pocetnu stranu ya logovanje
                    //Mora da se obeybedi i da se poruka ispice negde na pocetnoj strani
                    ViewBag.Message = "The mail you gave already exist. Try to log in with the given e-mail.";
                    return View(model);
                }
            }
            else
            {
                log.Info("User/Registration model.isValid error");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "Email, Password")] LoginExtended model)
        {
            if (ModelState.IsValid)
            {
                //hesiramo lozinku kako bi mogli da proverimo
                //da li nam se poklapa sa onom iz baze
                string password = "";
                using (MD5 md5Hash = MD5.Create())
                {
                    password = GetMd5Hash(md5Hash, model.Password);
                }

                Account account = myDB.Accounts.Where(a => a.Email == model.Email).FirstOrDefault();
                if (account != null)
                {
                    if (account.Password == password)
                    {
                        Session["User"] = account;
                        // return Content(String.Format("Pronaden korisnik mejl - {0} i lozinka - {1}", model.Email, model.Password));
                        return RedirectToAction("Index", "Auction");
                    }
                    else
                    {
                        ViewBag.Message = "Wrong password!";
                        model.Password = "";
                        return View(model);
                    }
                }
                else
                {
                    ViewBag.Message = "Account doesn't exist! if you want to join the auctions, please make an account.";
                    return RedirectToAction("Index", "Home");
                }

            }
            //Jos razmisli sta tu moze
            return View();
        }

        [UserAuthorized]
        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        [CurrentUserAuthorized]
        public ActionResult Details(int? id)
        {
            if (id.HasValue)
            {
                //provera da li trenutni korisnik gleda svoje podatke
                Account account = myDB.Accounts.Where(a => a.IdUser == id).FirstOrDefault();
                Account currentUser = Session["User"] as Account;

                if (account != null && currentUser.IdUser == account.IdUser)
                {
                    return View(account);
                }

                log.Info("User/Details User not found in DB");
                return HttpNotFound();
            }
            log.Info("User/Details no argument for method");
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [CurrentUserAuthorized]
        public ActionResult EditProfile(int? id)
        {
            if (id.HasValue)
            {
                //provera da li trenutni korisnik zeli da menja svoje podatke
                Account currentUser = Session["User"] as Account;
                Account myUser = myDB.Accounts.Where(a => a.IdUser == id).FirstOrDefault();

                if (currentUser.IdUser == myUser.IdUser)
                {
                    EditProfileExtended profile = new EditProfileExtended
                    {
                        FirstName = currentUser.FirstName,
                        LastName = currentUser.LastName,
                        Email = currentUser.Email,
                        Password = "",
                        ConfirmPassword = ""
                    };
                    return View(profile);
                }
                else
                {
                    log.Info("User/EditProfile User not found in DB");
                    return HttpNotFound();
                }
            }
            log.Info("User/EditProfile no argument for method");
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorized]
        public ActionResult EditProfile([Bind(Include = "FirstName, LastName, Email, Password, ConfirmPassword")] EditProfileExtended model)
        {
            if (ModelState.IsValid)
            {
                Account currentSession = Session["User"] as Account;
                Account foundAccount = myDB.Accounts.Where(a => a.IdUser == currentSession.IdUser).FirstOrDefault();

                if (foundAccount == null)
                {
                    return HttpNotFound();
                }

                if (model.Email != currentSession.Email && !validateEmail(model.Email))
                {
                    ViewBag.Message = "The mail you gave already exists";
                    return RedirectToAction("Index", "Home");
                }

                foundAccount.FirstName = model.FirstName;
                foundAccount.LastName = model.LastName;
                foundAccount.Email = model.Email;
                if (model.Password != null)
                {
                    using (MD5 md5Hash = MD5.Create())
                    {
                        foundAccount.Password = GetMd5Hash(md5Hash, model.ConfirmPassword);
                    }
                }
                myDB.Entry(foundAccount).State = EntityState.Modified;
                myDB.SaveChanges();

                Session["User"] = foundAccount;
                return RedirectToAction("Details", "User", new { id = foundAccount.IdUser });
            }
            return View(model);
        }

        [NonAction]
        public bool validateEmail(String email)
        {
            bool result = true;
            Account a = myDB.Accounts.Where(acc => acc.Email == email).FirstOrDefault();
            if (a != null)
                result = false;

            return result;
        }

        [NonAction]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                myDB.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
